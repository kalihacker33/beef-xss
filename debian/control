Source: beef-xss
Section: web
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Raphaël Hertzog <hertzog@debian.org>,
           Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 12),
               gem2deb,
               rake,
               ruby (>= 3.0),
               ruby-ansi,
               ruby-dev (>= 3.0),
               ruby-em-websocket (>= 0.3.6),
               ruby-erubis,
               ruby-espeak,
               ruby-eventmachine,
               ruby-execjs,
               ruby-json,
               ruby-maxmind-db,
               ruby-mime-types,
               ruby-msfrpc-client,
               ruby-parseconfig,
               ruby-qr4r,
               ruby-rack,
               ruby-rack-protection,
               ruby-rubydns (>= 0.7.0),
               ruby-sinatra,
               ruby-sqlite3,
               ruby-term-ansicolor,
               ruby-terser,
               ruby-twitter,
               ruby-zip (>= 1.0.0),
               rubygems-integration,
               thin,
Standards-Version: 4.6.0
Homepage: https://beefproject.com/
Vcs-Git: https://gitlab.com/kalilinux/packages/beef-xss.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/beef-xss
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: beef-xss
Architecture: amd64 i386 armhf arm64
XB-Ruby-Versions: ${ruby:Versions}
Depends: adduser,
         lsof,
         ${ruby:Depends},
         ruby-ansi,
         ruby-async-dns,
         ruby-dev,
         ruby-em-websocket (>= 0.3.6),
         ruby-erubis,
         ruby-espeak,
         ruby-eventmachine (>= 1.0.3),
         ruby-execjs,
         ruby-json,
         ruby-maxmind-db,
         ruby-mime-types,
         ruby-msfrpc-client,
         ruby-otr-activerecord,
         ruby-parseconfig,
         ruby-qr4r,
         ruby-rack (>= 1.4.1),
         ruby-rack-protection,
         ruby-rubydns (>= 0.7.0),
         ruby-rushover,
         ruby-sinatra (>= 1.4.2),
         ruby-slack-notifier,
         ruby-sqlite3,
         ruby-term-ansicolor,
         ruby-terser,
         ruby-twitter,
         ruby-xmlrpc,
         ruby-zip (>= 1.0.0),
         rubygems-integration,
         thin,
         xdg-utils,
         ${misc:Depends}
Recommends: geoipupdate
Breaks: kali-menu (<< 1.369)
Conflicts: beef-xss-bundle
Description: Browser Exploitation Framework (BeEF)
 BeEF is short for The Browser Exploitation Framework. It is a penetration
 testing tool that focuses on the web browser.
 .
 Amid growing concerns about web-born attacks against clients, including
 mobile clients, BeEF allows the professional penetration tester to assess the
 actual security posture of a target environment by using client-side attack
 vectors. Unlike other security frameworks, BeEF looks past the hardened
 network perimeter and client system, and examines exploitability within the
 context of the one open door: the web browser. BeEF will hook one or more web
 browsers and use them as beachheads for launching directed command modules and
 further attacks against the system from within the browser context.
